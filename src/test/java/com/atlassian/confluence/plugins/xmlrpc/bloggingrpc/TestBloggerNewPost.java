package com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestBloggerNewPost {
    @Mock
    private BloggingUtils bloggingUtils;

    @Mock
    private SpaceManager spaceManager;

    @Mock
    private SpacePermissionManager spacePermissionManager;

    @Mock
    private PageManager pageManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private ConfluenceUser user;

    @Mock
    private ApplicationProperties applicationProperties;

    private BloggerImpl bloggerImpl;

    @Before
    public void setUp() {
        bloggerImpl = new BloggerImpl(bloggingUtils, new TransactionTemplate() {
            @Override
            public <T> T execute(TransactionCallback<T> tTransactionCallback) {
                return tTransactionCallback.doInTransaction();
            }
        }, spaceManager, spacePermissionManager, pageManager, permissionManager, applicationProperties);

        when(bloggingUtils.getText(anyString(), Matchers.<String[]>anyObject())).thenAnswer(
                (Answer<String>) invocationOnMock -> invocationOnMock.getArguments()[0].toString()
        );

        when(bloggingUtils.getText(anyString())).thenAnswer(
                (Answer<String>) invocationOnMock -> invocationOnMock.getArguments()[0].toString()
        );
    }

    @After
    public void tearDown() {
        bloggingUtils = null;
        spaceManager = null;
        spacePermissionManager = null;
        pageManager = null;
        permissionManager = null;
        applicationProperties = null;
        user = null;
    }

    @Test
    public void testPostInNonExistentSpace() {
        try {
            bloggerImpl.newPost("", "TST", "", "", "", true);
            fail("RemoteException should've been raised");
        } catch (RemoteException re) {
            assertEquals("error.validation.space.unknown", re.getMessage());
        }
    }

    @Test
    public void testPostWithoutSufficientPrivileges() {
        String spaceKey = "TST";
        Space space = new Space(spaceKey);
        when(spaceManager.getSpace(spaceKey)).thenReturn(space);

        try {
            bloggerImpl.newPost("", spaceKey, "", "", "", true);
            fail("RemoteException should've been raised");
        } catch (RemoteException re) {
            assertEquals("error.permission.edit.space.blog", re.getMessage());
        }
    }

    @Test
    public void testPostWithoutTitle() throws RemoteException {
        String userName = "john.doe";
        String password = "";
        when(bloggingUtils.authenticateUser(userName, password)).thenReturn(user);

        String spaceKey = "TST";
        Space space = new Space(spaceKey);
        when(spaceManager.getSpace(spaceKey)).thenReturn(space);

        when(spacePermissionManager.hasPermission(SpacePermission.EDITBLOG_PERMISSION, space, user)).thenReturn(true);

        try {
            bloggerImpl.newPost("", spaceKey, userName, password, "Content", true);
            fail("RemoteException should've been raised");
        } catch (RemoteException re) {
            assertEquals("error.validation.blog.title.blank", re.getMessage());
        }
    }

    @Test
    public void testCreateDuplicateBlogPost() throws RemoteException {
        String userName = "john.doe";
        String password = "";
        when(bloggingUtils.authenticateUser(userName, password)).thenReturn(user);

        String spaceKey = "TST";
        Space space = new Space(spaceKey);
        when(spaceManager.getSpace(spaceKey)).thenReturn(space);

        when(spacePermissionManager.hasPermission(SpacePermission.EDITBLOG_PERMISSION, space, user)).thenReturn(true);
        when(pageManager.getBlogPost(eq(space.getKey()), eq("Title"), Matchers.anyObject())).thenReturn(new BlogPost());

        try {
            bloggerImpl.newPost("", spaceKey, userName, password, "<title>Title</title>Content", true);
            fail("RemoteException should've been raised");
        } catch (RemoteException re) {
            assertEquals("error.validation.blog.duplicate", re.getMessage());
        }
    }

    @Test
    public void testPostWithSufficientPrivileges() throws RemoteException {
        String userName = "john.doe";
        String password = "";
        when(bloggingUtils.authenticateUser(userName, password)).thenReturn(user);

        String spaceKey = "TST";
        Space space = new Space(spaceKey);
        when(spaceManager.getSpace(spaceKey)).thenReturn(space);

        when(spacePermissionManager.hasPermission(SpacePermission.EDITBLOG_PERMISSION, space, user)).thenReturn(true);

        bloggerImpl.newPost("", spaceKey, userName, password, "<title>Title</title>Content", true);
        verify(pageManager).saveContentEntity(argThat(
                post -> StringUtils.equals("Title", post.getTitle())
                        && StringUtils.equals("Content", post.getBodyAsString())
        ), isNull());
    }
}