package com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.rpc.RemoteException;

import java.util.Hashtable;
import java.util.Vector;

/**
 * Definition of the Blogger 1.0 API as an Interface. Refer to the <a
 * href="http://www.blogger.com/developers/api/1_docs/">Blogger API</a> for a
 * description of each methods functionality.
 *
 * @since 1.2.5
 */
public interface Blogger {

    Vector<Hashtable<String, String>> getUsersBlogs(String appKey, String username, String password)
            throws RemoteException;

    String newPost(String appKey, String blogid, String username, String password, String content,
                   boolean publish) throws RemoteException;

    boolean editPost(String appKey, String postid, String username, String password, String content,
                     boolean publish) throws RemoteException;

    Vector<Hashtable<String, Object>> getRecentPosts(String appkey, String blogid, String username,
                                                     String password, int numposts) throws RemoteException;

    boolean deletePost(String appkey, String postid, String username, String password,
                       boolean publish) throws RemoteException;

    boolean setTemplate(String appkey, String blogid, String username, String password,
                        String template, String templateType) throws RemoteException;

    String getTemplate(String appkey, String blogid, String username, String password,
                       String templateType) throws RemoteException;

    Hashtable<String, Object> getPost(String appkey, String postid, String username,
                                      String password) throws RemoteException;

    Hashtable<String, String> getUserInfo(String appkey, String username, String password)
            throws RemoteException;
}
